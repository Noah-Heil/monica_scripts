# Terraform-Automation

This is based on principles established [here](https://matklad.github.io//2021/02/06/ARCHITECTURE.md.html)

Describes the high-level architecture of the project.
Keep it short.
Specify only things which are unlikely to frequently change.
Don’t try to keep it synchronized with code. Instead, revisit it a couple of times a year.

Start with a bird’s eye overview of the problem being solved.

Then, specify a more-or-less detailed codemap.
Describe coarse-grained modules and how they relate to each other.
Codemap should answer “where’s the thing that does X?”.
Codemap should also answer “what does the thing that I am looking at do?”.

Avoid going into details of how each module works, pull this into separate documents or (better) inline documentation.
Codemap is a map of a country, and not an atlas of maps of its states.
Use this as a chance to reflect on the project structure.
Are the things you want to put near each other in the codemap adjacent?

Do name important files, modules, and types.
Do not directly link them (links go stale).
Instead, encourage the reader to use symbol search to find the mentioned entities by name.
This doesn’t require maintenance, and will help to discover related, similarly named things.

Explicitly call-out architectural invariants.
Often, important invariants are expressed as an absence of something, and it’s pretty hard to divine that from reading the code.
Nothing in the model layer specifically doesn’t depend on the views.

Point out boundaries between layers and systems as well.
A boundary implicitly contains information about the implementation of the system behind it.
It even constraints all possible implementations.
But finding a boundary by just randomly looking at the code is hard — good boundaries have measure zero.

After finishing the codemap, add a separate section on cross-cutting concerns.
