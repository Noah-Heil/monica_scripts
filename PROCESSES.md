# Processes

This document outlines processes and procedures for some common tasks in the repository.

## Deprecation

TBD

## Un-deprecation

TBD

## Promotion

TBD

## Reviewing A Pull Request

There are two parts to reviewing a pull request in the process to do so and the guidelines to follow. Both of those are outlined in the [Review Guidelines](REVIEW_GUIDELINES.md).
