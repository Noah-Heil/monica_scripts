# REVIEW_GUIDELINES

- Implement any changes in a seperate branch
- Create a pull request to the main branch (we follow more Trunk Based Development than Gitflow) explaining what changes you made
