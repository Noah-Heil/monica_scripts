# working_with_monicahq

## Bring Up

``` bash
mysqlCid="$(docker run -d \
 -e MYSQL_RANDOM_ROOT_PASSWORD=true \
 -e MYSQL_DATABASE=monica \
 -e MYSQL_USER=homestead \
 -e MYSQL_PASSWORD=secret \
 "mysql:5.7")"

docker run -d  --link "$mysqlCid":mysql  -e DB_HOST=mysql  -p 8080:80  monica
```

Now open up a browser window and navigate to `http://localhost:8080/`
Create the following user (this must be done by hand as the API does not currently support creating users yet):
(User Creation and API Key generation must be done by hand as the API does not currently support creating users or API Key programatically yet)

``` bash
# Email: staticus.mcfloofers@lazercatz.com
# FirstName: Staticus
# LastName: McFloofers
# password: lazercatz
```

Once logged in go to `http://localhost:8080/settings/import/upload` to upload the `dummyContacts.vcf` file to populate your contacts.
Then go to `http://localhost:8080/settings/api` and create a new personal access token named `staticus-mcfloofers`

once you have retrieved your personal access token you will be able to use it to execute requests against the API for example:

## Get all tags

``` bash
# We first need to export the our personal auth token into our environment so we dont go around accidentally pushing auth tokens into our repos and because everything looks cleaner as a result.
export MONICAHQ_AUTH_TOKEN="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMmZkZjViNzZlYTM0OTk0NWUzNjNiZjVmMjIzNTgzOGRmYzU5YTIyOGU4YTNkYzM1ZmZlNTgwZDIxNjUyN2VlOGU2YmM3NGJjYzFiN2I4YWYiLCJpYXQiOjE2MzYxMjg4NDQuNzg5Njg4LCJuYmYiOjE2MzYxMjg4NDQuNzg5Njk3LCJleHAiOjE2Njc2NjQ4NDQuNzg0MDQ4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.sZk176Ims1l9zt3A2h-sp_Hsh9AdPVn6PyfMuL8L7XZamjSXWqDlv52_i2XKY_On8Brjqmcw0EXV3fR6CM-BdWS8iWlIKFhfUFA2f0-7LJn5_nVWKJ22O7hU8GDczR1cyXDBbniB04y4B-0LFQuRauli5OSOv3aba5hrDa6LJbeRG5sF5Nz3DTXf8iYXJGUEU8M4dWHes2IfpKBaqGv6UFU6tNT0zW_PBhdFWpEbc4Z8atIiE1kaUgT-tGaafQg3Cb8iDkawoqMQPEkaWffH_xoz0jhzOj5UK4E_52tOOn6MaNHB5EUbUPDd61z7g7Sfa7TiB00BxAMYhieoHSmJS_tCPAXLqO_Y3BmocU4tWb62TUbMbf_c-MScY3VFmKySgmmDLFEcws9c9rgGEm0nK3RWaxtjZgzq1S9GWu9qfysERQbDjk-bvf2i25iD2t845afXRH43meZ0FGFgSqpblhwdmJLxUE8pTOMJh8i9GmqRj20YhJNvsLT8jsk3agYoxBnacXU5rNUw4fIpmdqQ-Bcbvi8qYIX7hHEQSCXY3khujHj8O4hPsUVAi89fZvSUiJNJyo7FWq2bMJ9VEW8IrwKL9Py7Lcq-ZSDmjEcT_8tjUA9qzA1J1CNgI4PQN4v67pyjtH0vk3sukRcH6_AV6gRRf99vdoLK9xjNl-0CDow"

# Make sure to set MONICAHQ_AUTH_TOKEN in your bash env before running.
MONICAHQ_AUTH_HEADER=""

printf -v MONICAHQ_AUTH_HEADER "Authorization: Bearer %s" "$MONICAHQ_AUTH_TOKEN"

# URI corresponding to the Monica instance you are attempting to execute against
MONICAHQ_URI="http://localhost:8080/api/tags"

# Execute the request
curl -H "$MONICAHQ_AUTH_HEADER" $MONICAHQ_URI

# This echo helps with the output looking nice.
echo ""
```

``` bash
# Or if you just need a quick one liner to test stuff out:
curl -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMmZkZjViNzZlYTM0OTk0NWUzNjNiZjVmMjIzNTgzOGRmYzU5YTIyOGU4YTNkYzM1ZmZlNTgwZDIxNjUyN2VlOGU2YmM3NGJjYzFiN2I4YWYiLCJpYXQiOjE2MzYxMjg4NDQuNzg5Njg4LCJuYmYiOjE2MzYxMjg4NDQuNzg5Njk3LCJleHAiOjE2Njc2NjQ4NDQuNzg0MDQ4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.sZk176Ims1l9zt3A2h-sp_Hsh9AdPVn6PyfMuL8L7XZamjSXWqDlv52_i2XKY_On8Brjqmcw0EXV3fR6CM-BdWS8iWlIKFhfUFA2f0-7LJn5_nVWKJ22O7hU8GDczR1cyXDBbniB04y4B-0LFQuRauli5OSOv3aba5hrDa6LJbeRG5sF5Nz3DTXf8iYXJGUEU8M4dWHes2IfpKBaqGv6UFU6tNT0zW_PBhdFWpEbc4Z8atIiE1kaUgT-tGaafQg3Cb8iDkawoqMQPEkaWffH_xoz0jhzOj5UK4E_52tOOn6MaNHB5EUbUPDd61z7g7Sfa7TiB00BxAMYhieoHSmJS_tCPAXLqO_Y3BmocU4tWb62TUbMbf_c-MScY3VFmKySgmmDLFEcws9c9rgGEm0nK3RWaxtjZgzq1S9GWu9qfysERQbDjk-bvf2i25iD2t845afXRH43meZ0FGFgSqpblhwdmJLxUE8pTOMJh8i9GmqRj20YhJNvsLT8jsk3agYoxBnacXU5rNUw4fIpmdqQ-Bcbvi8qYIX7hHEQSCXY3khujHj8O4hPsUVAi89fZvSUiJNJyo7FWq2bMJ9VEW8IrwKL9Py7Lcq-ZSDmjEcT_8tjUA9qzA1J1CNgI4PQN4v67pyjtH0vk3sukRcH6_AV6gRRf99vdoLK9xjNl-0CDow" http://localhost:8080/api/tags
```

For additional information about the API you can reference `https://www.monicahq.com/api`
