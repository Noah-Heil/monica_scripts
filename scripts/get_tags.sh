# Make sure to set MONICAHQ_AUTH_TOKEN in your bash env before running.
MONICAHQ_AUTH_HEADER=""

printf -v MONICAHQ_AUTH_HEADER "Authorization: Bearer %s" "$MONICAHQ_AUTH_TOKEN"

# URI corresponding to the Monica instance you are attempting to execute against
MONICAHQ_URI="http://localhost:8080/api/tags"

# Execute the request
curl -H "$MONICAHQ_AUTH_HEADER" $MONICAHQ_URI

# This echo helps with the output looking nice.
echo ""